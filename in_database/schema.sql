-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS doctors;
DROP TABLE IF EXISTS locations;
DROP TABLE IF EXISTS doctor_locations;
DROP TABLE IF EXISTS doctor_schedule;

CREATE TABLE doctors (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL
);

CREATE TABLE locations (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  address TEXT NOT NULL
);

CREATE TABLE doctor_locations (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  doctor_id INTEGER NOT NULL,
  location_id INTEGER NOT NULL,
  FOREIGN KEY (doctor_id) REFERENCES doctors (id),
  FOREIGN KEY (location_id) REFERENCES locations (id)
);

CREATE TABLE doctor_schedule (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  doctor_id INTEGER NOT NULL,
  time TEXT NOT NULL,
  available INTEGER NOT NULL,
  location_id INTEGER,
  FOREIGN KEY (doctor_id) REFERENCES doctors (id),
  FOREIGN KEY (location_id) REFERENCES locations (id)
);


INSERT INTO doctors(id, first_name, last_name) VALUES (0, 'John', 'Doe');
INSERT INTO doctors(id, first_name, last_name) VALUES (1, 'Jane', 'Smith');

INSERT INTO locations(id, address) VALUES (0, '123 Main St');
INSERT INTO locations(id, address) VALUES (1, '456 Central St');

INSERT INTO doctor_locations(id, doctor_id, location_id) VALUES (0, 0, 0);
INSERT INTO doctor_locations(id, doctor_id, location_id) VALUES (1, 1, 0);
INSERT INTO doctor_locations(id, doctor_id, location_id) VALUES (2, 1, 1);

INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (0, 0, "2020-05-01 09:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (1, 0, "2020-05-02 09:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (2, 0, "2020-05-03 11:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (3, 0, "2020-05-05 11:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (4, 1, "2020-05-01 13:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (5, 1, "2020-05-02 13:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (6, 1, "2020-05-03 09:00:00", 1);
INSERT INTO doctor_schedule(id, doctor_id, time, available) VALUES (7, 1, "2020-05-04 11:00:00", 1);
