import os

from flask import Flask, jsonify, request
from in_database import db


# Program & structure influenced heavily by the Flask tutorial
# http://flask.pocoo.org/docs/1.0/tutorial/database/
def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY='dev',
        # store the database in the instance folder
        DATABASE=os.path.join(app.instance_path, 'doctors.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # register the database commands
    db.init_app(app)

    @app.route('/doctors', methods=['GET'])
    def list_doctors():
        """
        Get all doctors

        :return: List of full doctor rows
        """
        cursor = db.get_db().cursor()

        result = cursor.execute(
            'SELECT id, first_name, last_name '
            'FROM doctors'
        ).fetchall()

        # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
        doctors = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

        cursor.close()

        return jsonify(doctors), 200

    @app.route('/doctors/<int:doctor_id>', methods=['GET'])
    def list_doctor(doctor_id):
        """
        Get one doctor

        :param doctor_id: The id of the doctor
        :return: Full doctor row
        """
        cursor = db.get_db().cursor()

        result = cursor.execute(
            'SELECT id, first_name, last_name '
            'FROM doctors '
            'WHERE id = ?',
            (doctor_id, )
        ).fetchone()

        if result is None:
            cursor.close()
            return jsonify({'error_detail': 'Doctor not found'}), 404

        # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
        doctor = dict(zip([key[0] for key in cursor.description], result))

        cursor.close()

        return jsonify(doctor), 200

    # Note: Must set the content type to JSON. Use something like:
    # curl -X PUT -H "Content-Type: application/json" --data '{"first_name": "Bob", "last_name": "Robert", "location_ids": "0,1"}' http://localhost:5000/doctors/0
    @app.route('/doctors/<int:doctor_id>', methods=['PUT'])
    def update_doctor(doctor_id):
        """
        Update a doctor's info. This will also set the doctor's locations.
        This is a replace opperation, not an update, so all values should be
        accounted for.

        :param doctor_id: The doctor's ID
        :param first_name: The doctor's first name
        :param last_name: The doctor's last name
        :param location_ids: Comma sepparated list of location IDs

        :return: The new details of the doctor, including locations
        """

        # I think updating the doctor's info and locations should
        # be done as separate endpoints, but I'm combining this into one
        # for the sake of time.
        # And the location_ids would be multiple parameters of the same name
        # but I wasn't quite sure how to do that.

        req_data = request.get_json()

        try:
            first_name = req_data['first_name']
            last_name = req_data['last_name']
            location_ids =  req_data['location_ids'].split(',')
        except KeyError:
            return jsonify({'error_detail': 'Missing required field'}), 400

        try:
            cursor = db.get_db().cursor()

            # update doctor's info
            cursor.execute(
                'UPDATE doctors '
                'SET first_name = ?, '
                '   last_name = ? '
                'WHERE id = ? ',
                (first_name, last_name, doctor_id)
            )

            # remove previous locations
            cursor.execute(
                'DELETE FROM doctor_locations '
                'WHERE doctor_id = ? ',
                (doctor_id, )
            )

            # insert new locations
            for location_id in location_ids:
                cursor.execute(
                    'INSERT INTO doctor_locations (doctor_id, location_id) '
                    'VALUES (?, ?)',
                    (doctor_id, location_id)
                )

            # get update details about doctor, including locations
            result = cursor.execute(
                'SELECT dl.id as location_id, first_name, last_name, l.address '
                'FROM doctor_locations dl '
                'INNER JOIN locations l ON dl.location_id = l.id '
                'INNER JOIN doctors d ON dl.doctor_id = d.id '
                'WHERE dl.doctor_id = ?',
                (doctor_id,)
            ).fetchall()

            # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
            doctor = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

            cursor.close()
            db.get_db().commit()

            return jsonify(doctor), 200
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400

    # Use something like:
    # curl -X DELETE http://localhost:5000/doctors/0
    @app.route('/doctors/<int:doctor_id>', methods=['DELETE'])
    def delete_doctor(doctor_id):
        """
        Remove a doctor from the scheduling system

        :param doctor_id: The doctor's ID

        :return: An empty response if the request was successful
        """

        try:
            cursor = db.get_db().cursor()

            # check to see if doctor exists in the system
            exists = cursor.execute(
                'SELECT id '
                'FROM doctors '
                'WHERE id = ?',
                (doctor_id,)
            ).fetchone()

            # any DELETE operation on a resource that doesn't exist should return a 404
            if exists is None:
                cursor.close()
                return jsonify({'error_detail': 'doctor does not exist in the system'}), 404

            # remove doctor's info
            cursor.execute(
                'DELETE FROM doctors '
                'WHERE id = ? ',
                (doctor_id, )
            )

            # remove previous locations
            cursor.execute(
                'DELETE FROM doctor_locations '
                'WHERE doctor_id = ? ',
                (doctor_id, )
            )

            # remove schedule
            cursor.execute(
                'DELETE FROM doctor_schedule '
                'WHERE doctor_id = ? ',
                (doctor_id, )
            )

            cursor.close()
            db.get_db().commit()

            return '{}', 200
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400


    # Note: Must set the content type to JSON. Use something like:
    # curl -X POST -H "Content-Type: application/json" --data '{"first_name": "Joe", "last_name": "Smith"}' http://localhost:5000/doctors
    @app.route('/doctors', methods=['POST'])
    def add_doctor():
        """
        Create a doctor

        :param first_name: The doctor's first name
        :param last_name: The doctor's last name

        :return: The id of the newly created doctor
        """
        req_data = request.get_json()

        # check to see if the input data is valid
        try:
            first_name = req_data['first_name']
            last_name = req_data['last_name']
        except KeyError:
            return jsonify({'error_detail': 'Missing required field'}), 400

        try:
            cursor = db.get_db().cursor()

            # insert new doctor into system
            cursor.execute(
                'INSERT INTO doctors (first_name, last_name) '
                'VALUES (?, ?)',
                (first_name, last_name)
            )

            doctor_id = cursor.lastrowid

            cursor.close()
            db.get_db().commit()
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400

        return jsonify({'id': doctor_id}), 200


    @app.route('/doctors/<int:doctor_id>/locations', methods=['GET'])
    def list_doctor_locations(doctor_id):
        """
        Get the locations for a single doctor

        :param doctor_id: The id of the doctor
        :return: List of full location rows
        """

        cursor = db.get_db().cursor()

        result = cursor.execute(
            'SELECT l.id, l.address '
            'FROM doctor_locations dl '
            'INNER JOIN locations l ON dl.location_id = l.id '
            'WHERE dl.doctor_id = ?',
            (doctor_id,)
        ).fetchall()

        # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
        locations = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

        cursor.close()

        return jsonify(locations), 200

    @app.route('/doctors/<int:doctor_id>/schedule', methods=['GET'])
    def list_doctor_schedule(doctor_id):
        """
        Get the schedule for a single doctor

        :param doctor_id: The id of the doctor
        :return: List of full schedule rows
        """

        cursor = db.get_db().cursor()

        result = cursor.execute(
            'SELECT ds.id, ds.time, ds.available, l.address '
            'FROM doctor_schedule ds '
            'LEFT JOIN locations l ON ds.location_id = l.id '
            'WHERE ds.doctor_id = ?',
            (doctor_id, )
        ).fetchall()

        # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
        schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

        cursor.close()

        return jsonify(schedule), 200


    # Note: Must set the content type to JSON. Use something like:
    # curl -X POST -H "Content-Type: application/json" --data '{"time": "2020-05-01 09:00:00", "location_id": 1}' http://localhost:5000/doctors/1/schedule
    @app.route('/doctors/<int:doctor_id>/schedule', methods=['POST'])
    def create_doctor_appointment(doctor_id):
        """
        Schedule a doctors appointment

        :param doctor_id: The doctor's ID
        :param time: The appointment time
        :param location_id: The location's ID

        :return: The details of the appointment time
        """
        req_data = request.get_json()

        # check the required data exists
        try:
            time = req_data['time']
            location_id = req_data['location_id']
        except KeyError:
            return jsonify({'error_detail': 'Missing required field'}), 400

        try:
            cursor = db.get_db().cursor()

            # get the current row that match the input data
            result = cursor.execute(
                'SELECT id, time, available '
                'FROM doctor_schedule  '
                'WHERE doctor_id = ? '
                'AND available = 1 '
                'AND time = ?',
                (doctor_id, time, )
            ).fetchall()

            schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

            # verify the appointment time is available
            if len(schedule) != 0:
                # update availability
                cursor.execute(
                    'UPDATE doctor_schedule '
                    'SET available = 0,'
                    '   location_id = ? '
                    'WHERE doctor_id = ? '
                    'AND time = ?',
                    (location_id, doctor_id, time)
                )

                # get new values for time slot
                result = cursor.execute(
                    'SELECT ds.id, ds.time, ds.available, l.address '
                    'FROM doctor_schedule ds '
                    'LEFT JOIN locations l ON ds.location_id = l.id '
                    'WHERE doctor_id = ? '
                    'AND time = ?',
                    (doctor_id, time, )
                ).fetchall()

                schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

                cursor.close()
                db.get_db().commit()

                # return new values for the time slot
                return jsonify(schedule), 200
            else:
                cursor.close()
                return jsonify({'error_detail': 'appointment time not available'}), 400
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400


    # Note: Must set the content type to JSON. Use something like:
    # curl -X DELETE -H "Content-Type: application/json" --data '{"time": "2020-05-01 09:00:00", "location_id": 1}' http://localhost:5000/doctors/1/schedule
    @app.route('/doctors/<int:doctor_id>/schedule', methods=['DELETE'])
    def cancel_doctor_appointment(doctor_id):
        """
        Cancel a doctors appointment

        :param doctor_id: The doctor's ID
        :param time: The appointment time
        :param location_id: The location's ID

        :return: The new details of the canceled appointment time
        """
        req_data = request.get_json()

        # check the required data exists
        try:
            time = req_data['time']
            location_id = req_data['location_id']
        except KeyError:
            return jsonify({'error_detail': 'Missing required field'}), 400

        try:
            cursor = db.get_db().cursor()

            # get the current row that match the input data
            result = cursor.execute(
                'SELECT id, time, available '
                'FROM doctor_schedule  '
                'WHERE doctor_id = ? '
                'AND available = 0 '
                'AND time = ? '
                'AND location_id = ?',
                (doctor_id, time, location_id)
            ).fetchall()

            schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

            # verify the appointment time
            if len(schedule) != 0:
                # update availability
                cursor.execute(
                    'UPDATE doctor_schedule '
                    'SET available = 1,'
                    '   location_id = NULL '
                    'WHERE doctor_id = ? '
                    'AND time = ? '
                    'AND location_id = ?',
                    (doctor_id, time, location_id)
                )

                # get new values for time slot
                result = cursor.execute(
                    'SELECT ds.id, ds.time, ds.available '
                    'FROM doctor_schedule ds '
                    'LEFT JOIN locations l ON ds.location_id = l.id '
                    'WHERE doctor_id = ? '
                    'AND time = ?',
                    (doctor_id, time, )
                ).fetchall()

                schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

                cursor.close()
                db.get_db().commit()

                # return new values for the time slot
                return jsonify(schedule), 200
            else:
                cursor.close()
                return jsonify({'error_detail': "appointment time doesn't exist or has already been canceled"}), 400
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400

    # Note: Must set the content type to JSON. Use something like:
    # curl -X POST -H "Content-Type: application/json" --data '{"time": "2020-05-01 09:00:00"}' http://localhost:5000/doctors/1/available
    @app.route('/doctors/<int:doctor_id>/available', methods=['POST'])
    def create_doctor_availability(doctor_id):
        """
        Create a time slot that a doctor would be available

        :param doctor_id: The doctor's ID
        :param time: The appointment time

        :return: The details of the time slot
        """
        req_data = request.get_json()

        # I'm not sure if I like having a separate endpoint for
        # creating a time slot for an available appointment.
        # It might be cleaner to have it under the /schedule
        # endpoint

        # check the required data exists
        try:
            time = req_data['time']
        except KeyError:
            return jsonify({'error_detail': 'Missing required field'}), 400

        try:
            cursor = db.get_db().cursor()

            # check to see if the time slot is taken
            free = cursor.execute(
                'SELECT * '
                'FROM doctor_schedule  '
                'WHERE doctor_id = ? '
                'AND time = ? ',
                (doctor_id, time)
            ).fetchone()

            # verify the appointment time
            if free is None:
                # update availability
                cursor.execute(
                    'INSERT INTO doctor_schedule (doctor_id, time, available) '
                    'VALUES (?, ?, 1)',
                    (doctor_id, time)
                )

                # get new values for time slot
                result = cursor.execute(
                    'SELECT id, time, available '
                    'FROM doctor_schedule '
                    'WHERE doctor_id = ?',
                    (doctor_id, )
                ).fetchall()

                # See https://medium.com/@PyGuyCharles/python-sql-to-json-and-beyond-3e3a36d32853
                schedule = [dict(zip([key[0] for key in cursor.description], row)) for row in result]

                cursor.close()
                db.get_db().commit()

                # return new values for the time slot
                return jsonify(schedule), 200
            else:
                cursor.close()
                return jsonify({'error_detail': "time slot is not available"}), 400
        except Exception as e:
            return jsonify({'error_detail': e.message}), 400

    return app
