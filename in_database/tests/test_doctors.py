import json


def test_get_all_doctors(client):
    # Test that getting all doctors truly gets them all
    rv = client.get('/doctors')
    assert rv.status_code == 200

    # Can't guarantee order, so test that we get the expected count and fields seem to make sense
    data = json.loads(rv.data)
    assert len(data) == 2
    for field in ['id', 'first_name', 'last_name']:
        assert field in data[0]


def test_get_valid_doctor(client):
    # Test getting a single doctor, successfully
    rv = client.get('/doctors/0')
    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert data['id'] == 0
    assert data['first_name'] == 'Testy'
    assert data['last_name'] == 'McTestFace'


def test_get_invalid_doctor(client):
    # Test getting a single doctor that doesn't exist
    rv = client.get('/doctors/2')
    assert rv.status_code == 404


def test_create_doctor(client):
    # Test creating a real doctor, successfully

    # Note: Flask chokes if you pass in an inline dict; must use json.dumps()
    rv = client.post('/doctors',
                     data=json.dumps(dict(first_name='Elmer', last_name='Hartman')),
                     content_type='application/json')

    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert data['id'] == 2


def test_create_invalid_doctor(client):
    # Test various ways a doctor creation may fail
    rv = client.post('/doctors',
                     data=json.dumps(dict(first_name='Elmer')),
                     content_type='application/json')

    assert rv.status_code == 400

    data = json.loads(rv.data)
    assert data['error_detail'] == 'Missing required field'

    rv = client.post('/doctors',
                     data=json.dumps(dict(last_name='Hartman')),
                     content_type='application/json')

    assert rv.status_code == 400

    data = json.loads(rv.data)
    assert data['error_detail'] == 'Missing required field'

def test_update_doctor(client):
    # Test updating a doctor, successfully
    rv = client.put('/doctors/0',
                     data=json.dumps(dict(first_name='Elmer', last_name='Fudge', location_ids='0,1')),
                     content_type='application/json')

    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert len(data) == 2
    assert data[0]['first_name'] == 'Elmer'
    assert data[0]['last_name'] == 'Fudge'
    assert 'location_id' in data[0]
    assert 'address' in data[0]

def test_delete_doctor(client):
    # Test removing doctor, successfully
    rv = client.delete('/doctors/0')

    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert len(data) == 0

def test_invalid_delete_doctor(client):
    # Test removing a doctor that doesn't exist
    rv = client.delete('/doctors/100')

    assert rv.status_code == 404

def test_get_schedule(client):
    # Test getting a single doctors schedule, successfully
    rv = client.get('/doctors/0/schedule')
    assert rv.status_code == 200

    # Can't guarantee order, so test that we get the expected count and fields seem to make sense
    data = json.loads(rv.data)
    assert len(data) == 2
    for field in ['id', 'available', 'time']:
        assert field in data[0]


def test_create_appointment(client):
    # Test creating an appointment, successfully
    rv = client.post('/doctors/0/schedule',
                     data=json.dumps(dict(time='2020-05-04 11:00:00', location_id=0)),
                     content_type='application/json')
    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert len(data) == 1
    assert data[0]['id'] == 0
    assert data[0]['available'] == 0
    assert data[0]['time'] == '2020-05-04 11:00:00'
    assert data[0]['address'] == '1 Park St'


def test_cancel_appointment(client):
    # Test cancel an appointment, successfully
    rv = client.delete('/doctors/0/schedule',
                     data=json.dumps(dict(time='2020-05-05 09:00:00', location_id=1)),
                     content_type='application/json')
    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert len(data) == 1
    assert data[0]['available'] == 1
    assert data[0]['time'] == '2020-05-05 09:00:00'

def test_schedule_availability(client):
    rv = client.post('/doctors/1/available',
                     data=json.dumps(dict(time='2020-05-06 11:00:00')),
                     content_type='application/json')
    assert rv.status_code == 200

    data = json.loads(rv.data)
    assert len(data) == 1
    assert data[0]['available'] == 1
    assert data[0]['time'] == '2020-05-06 11:00:00'